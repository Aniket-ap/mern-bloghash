import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { getUser, updateUser, updateUserNameOnMenu } from "../auth";
import DefaultAvatar from "../assets/images/defaultAvatar.png";

const EditProfile = (props) => {
  const [userValues, setUserValues] = useState({
    id: "",
    name: "",
    email: "",
    password: "",
    about: "",
    error: "",
  });

  const [redirectToSignin, setRedirectToSignin] = useState(false);
  let [userData] = useState(new FormData());
  const [fileSize, setFileSize] = useState(0);

  useEffect(() => {
    // userData = new FormData();
    const userId = props.match.params.userId;
    getUser(userId)
      .then((res) => {
        const data = res?.data;
        if (data.error) {
          setRedirectToSignin(true);
        } else {
          setUserValues({
            ...userValues,
            id: data._id,
            name: data.name,
            email: data.email,
            about: data.about
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  function handleInputChange(event) {
    setUserValues({ ...userValues, error: "" });
    const value =
      event.target.name === "photo"
        ? event.target.files[0]
        : event.target.value;

    userData.set(event.target.name, value);
    setFileSize(event.target.name === "photo" ? event.target.files[0].size : 0);
    setUserValues({ ...userValues, [event.target.name]: value });
  }

  const onSubmitHandler = (event) => {
    event.preventDefault();
    // const { name, email, password } = userValues;

    if (isValid()) {
      // const user = {
      //   name,
      //   email,
      //   password: password || undefined,
      // };

      const userId = props.match.params.userId;

      updateUser(userId, userData)
        .then((res) => {
          // if(res.data.error) setUserValues({...userValues, error: res.data.error})
          // else setRedirectToSignin(true)
          updateUserNameOnMenu(res.data, () => {setRedirectToSignin(true)})
          // setRedirectToSignin(true);
        })
        .catch((err) => {
          setUserValues((values) => ({
            ...userValues,
            error: err?.response?.data.error,
          }));
        });
    }
  };

  const isValid = () => {
    const { name, email, password } = userValues;

    if (fileSize > 100000) {
      setUserValues({
        ...userValues,
        error: "File size should be less then 100kb",
      });
      return false;
    }

    if (name.length === 0) {
      setUserValues({ ...userValues, error: "Name is required" });
      return false;
    }
    // email@domain.com
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      setUserValues({ ...userValues, error: "A valid Email is required" });
      return false;
    }
    if (password.length >= 1 && password.length <= 5) {
      setUserValues({
        ...userValues,
        error: "Password must be at least 6 characters long",
      });
      return false;
    }
    return true;
  };

  const signUpForm = () => (
    <form>
      <div className="form-group p-2">
        <label className="text-muted">Profile Photo</label>
        <input
          type="file"
          className="form-control"
          name="photo"
          accept="image/*"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Name</label>
        <input
          type="text"
          className="form-control"
          name="name"
          value={userValues.name}
          placeholder="Enter name"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Email</label>
        <input
          type="email"
          className="form-control"
          name="email"
          value={userValues.email}
          placeholder="Enter email"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">About</label>
        <textarea
          type="text"
          className="form-control"
          name="about"
          value={userValues.about}
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Password</label>
        <input
          type="password"
          className="form-control"
          name="password"
          value={userValues.password}
          placeholder="Enter password"
          onChange={handleInputChange}
        />
      </div>
      <button onClick={onSubmitHandler} className="btn btn-primary mt-2">
        Update
      </button>
    </form>
  );

  if (redirectToSignin) {
    return <Redirect to={`/user/${userValues.id}`} />;
  }

  const photoUrl = userValues.id
    ? `${process.env.REACT_APP_API_URL}/user/photo/${userValues.id}?${new Date().getTime()}`
    : DefaultAvatar;

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">EditProfile</h2>

      {userValues.error ? (
        <div className="alert alert-danger" role="alert">
          {userValues.error}
        </div>
      ) : (
        ""
      )}

      <img style={{height:"200px", width:"auto"}} className="img-thumbnail" src={photoUrl} alt={userValues.name} />

      {signUpForm()}
    </div>
  );
};

export default EditProfile;
