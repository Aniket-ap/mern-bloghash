import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import {  userDelete } from "../auth";
import { signout } from "../core/Menu";

const DeleteUser = (props) => {
  const [redirect, setRedirect] = useState(false);

  const deleteAccount = () => {
    const userId = props.userId;

    userDelete(userId)
      .then((res) => {
        const data = res?.data;
        if (data.error) {
          console.log(data.error);
        } else {
          // signout user
          signout(() => console.log("user deleted"));
          // redirecft
          setRedirect(true);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const confirmDelete = () => {
    let answer = window.confirm(
      "Are you sure you want to delete your account ?"
    );
    if (answer) {
      deleteAccount();
    }
  };

  if (redirect) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <button
        onClick={confirmDelete}
        className="btn btn-danger"
      >
        Delete Profile
      </button>
    </div>
  );
};

export default DeleteUser;
