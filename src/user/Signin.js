import React, { useState } from "react";
// import axios from "axios";
import { Redirect } from "react-router-dom";
import { signInUser } from "../auth";

const Signin = () => {
  const [values, setValues] = useState({
    email: "",
    password: "",
    error: "",
  });
  const [loading, setLoading] = useState(false);

  const [redirectToRefer, setRedirectToRefer] = useState(false);

  const handleInputChange = (event) => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const isValid = () => {
    const { email, password } = values;

    // email@domain.com
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      setValues({ ...values, error: "A valid Email is required" });
      return false;
    }

    if (password.length === 0) {
      setValues({
        ...values,
        error: "Password must not be empty",
      });
      return false;
    }

    if (password.length >= 1 && password.length <= 5) {
      setValues({
        ...values,
        error: "Password must be at least 6 characters long",
      });
      return false;
    }

    return true;
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();

    const { email, password } = values;

    const user = {
      email,
      password,
    };

    if (isValid()) {
      setLoading(true);
      signInUser(user)
        .then((res) => {
          const data = res?.data;
          setValues((values) => ({
            ...values,
            email: "",
            password: "",
            error: "",
          }));
          // Authenticate
          authenticate(data, () => {
            setRedirectToRefer(true);
          });
        })
        .catch((err) => {
          console.log(err.response);
          setValues((values) => ({
            ...values,
            error: err?.response?.data?.error,
          }));
          setLoading(false);
        });
    }
  };

  const authenticate = (jwt, next) => {
    if (typeof window !== "undefined") {
      localStorage.setItem("jwt", JSON.stringify(jwt));
      next();
    }
  };

  const signInForm = () => (
    <form>
      <div className="form-group p-2">
        <label className="text-muted">Email</label>
        <input
          type="email"
          className="form-control"
          name="email"
          value={values.email}
          placeholder="Enter email"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Password</label>
        <input
          type="password"
          className="form-control"
          name="password"
          value={values.password}
          placeholder="Enter password"
          onChange={handleInputChange}
        />
      </div>
      <button onClick={onSubmitHandler} className="btn btn-primary mt-2">
        Submit
      </button>
    </form>
  );

  if (redirectToRefer) {
    return <Redirect to="/" />;
  }

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">Signin</h2>

      {values.error ? (
        <div className="alert alert-danger" role="alert">
          {values.error}
        </div>
      ) : (
        ""
      )}

      {loading ? (
        <div className="d-flex align-items-center">
          <strong>Loading...</strong>
          <div
            className="spinner-border ms-auto"
            role="status"
            aria-hidden="true"
          ></div>
        </div>
      ) : (
        signInForm()
      )}

      {/* {signInForm()} */}
    </div>
  );
};

export default Signin;
