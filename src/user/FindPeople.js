import React, { useState, useEffect } from "react";
import { findPeople, isAuthenticated, follow } from "../auth";
import defaultAvatar from "../assets/images/defaultAvatar.png";
import { Link } from "react-router-dom";

const FindPeople = () => {
  const [users, setUsers] = useState({
    users: [],
    error: "",
    open: false,
    followMessage: "",
  });

  useEffect(() => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;
    findPeople(userId, token)
      .then((res) => {
        const data = res?.data;
        if (data.error) {
          console.log(data.error);
        } else {
          setUsers(data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const clickFollow = (user, i) => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;

    follow(userId, token, user._id).then((data) => {
      if (data.error) {
        setUsers({ ...users, error: data.error });
      } else {
        let toFollow = this.state.users;
        toFollow.splice(i, 1);
        setUsers({
          ...users,
          users: toFollow,
          open: true,
          followMessage: `Following ${user.name}`,
        });
      }
    });
  };

  const populateUser = (users) => (
    <div className="row gy-3">
      {users.users.map((user, i) => (
        <div className="card col-md-4" key={i}>
          <img
            style={{ height: "200px", width: "auto", objectFit: "cover" }}
            className="img-thumbnail"
            src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}`}
            onError={(i) => (i.target.src = `${defaultAvatar}`)}
            alt={user.name}
          />
          <div className="card-body">
            <h5 className="card-title">{user.name}</h5>
            <p className="card-text">{user.email}</p>
            <Link to={`/user/${user._id}`} className="btn btn-primary">
              View Profile
            </Link>
            <button
              onClick={() => clickFollow(user, i)}
              className="btn btn-raised btn-info float-right btn-sm"
            >
              Follow
            </button>
          </div>
        </div>
      ))}
    </div>
  );

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">FindPeople</h2>

      {users.open && (
        <div className="alert alert-success">{users.followMessage}</div>
      )}

      {populateUser(users)}
    </div>
  );
};

export default FindPeople;
