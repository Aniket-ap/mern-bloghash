import React from "react";
import {follow, unfollow} from "../auth/index"

const FollowProfileBtn = (props) => {

    const followClick = () => {
        props.onBtnClick(follow)
    }
    const unFollowClick = () => {
        props.onBtnClick(unfollow)
    }

  return (
    <div className="d-inline-block mt-3">
      {!props.following ? (
        <button onClick={followClick} className="btn btn-success" style={{ marginRight: "5px" }}>
          Follow
        </button>
      ) : (
        <button onClick={unFollowClick} className="btn btn-warning ">UnFollow</button>
      )}
    </div>
  );
};

export default FollowProfileBtn;
