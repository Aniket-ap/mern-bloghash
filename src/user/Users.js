import React, { useState, useEffect } from "react";
import { getAllUsers } from "../auth";
import defaultAvatar from "../assets/images/defaultAvatar.png";
import { Link } from "react-router-dom";

const Users = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getAllUsers()
      .then((res) => {
        const data = res?.data;
        if (data.error) {
          console.log(data.error);
        } else {
          setUsers(data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const populateUser = (users) => (
    <div className="row gy-3">
      {users.map((user, i) => (
        <div className="card col-md-4" key={i}>
          <img
            style={{ height: "200px", width: "auto",objectFit:"cover" }}
            className="img-thumbnail"
            src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}`}
            onError={i => (i.target.src = `${defaultAvatar}`)}
            alt={user.name}
          />
          <div className="card-body">
            <h5 className="card-title">{user.name}</h5>
            <p className="card-text">{user.email}</p>
            <Link to={`/user/${user._id}`} className="btn btn-primary">
              View Profile
            </Link>
          </div>
        </div>
      ))}
    </div>
  );

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">Users</h2>

      {populateUser(users)}
    </div>
  );
};

export default Users;
