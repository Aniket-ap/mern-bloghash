import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { isAuthenticated, getUser } from "../auth";
import defaultAvatar from "../assets/images/defaultAvatar.png";
import DeleteUser from "./DeleteUser";
import FollowProfileBtn from "./FollowProfileBtn";
import ProfilesTab from "./ProfilesTab";
import { listByUser } from "../post/apiPost";

const Profile = (props) => {
  const [user, setUser] = useState({ following: [], followers: [] });
  const [redirectToSignin, setRedirectToSignin] = useState(false);
  const [following, setFollowing] = useState(false);
  const [error, setError] = useState("");
  const [posts, setPosts] = useState([]);

  const checkFollow = (user) => {
    const jwt = isAuthenticated();
    const match = user.followers.find((follower) => {
      return follower._id === jwt.user._id;
    });
    return match;
  };

  const clickFollowButton = (followApiCall) => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;

    followApiCall(userId, token, user._id).then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setUser(data);
        setFollowing(!following);
      }
    });
  };

  const loadPosts = (userId) => {
    const token = isAuthenticated().token

    listByUser(userId, token).then(data => {
      if(data.error){
        console.log(data.error)
      } else{
        setPosts(data)
      }
    })
  }

  useEffect(() => {
    const userId = props.match.params.userId;
    getUser(userId)
      .then((res) => {
        const data = res?.data;
        if (data.error) {
          setRedirectToSignin(true);
        } else {
          let following = checkFollow(data);
          setFollowing(following);
          setUser(data);
          loadPosts(data._id)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props.match.params.userId]);

  if (redirectToSignin) {
    return <Redirect to="/signin" />;
  }

  const photoUrl = user._id
    ? `${process.env.REACT_APP_API_URL}/user/photo/${
        user._id
      }?${new Date().getTime()}`
    : defaultAvatar;

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">Profile</h2>
      <div className="row">
        <div className="col-md-4">
          <img
            style={{ height: "200px", width: "auto" }}
            className="img-thumbnail"
            src={photoUrl}
            onError={(i) => (i.target.src = `${defaultAvatar}`)}
            alt={user.name}
          />
        </div>
        <div className="col-md-8">
          <div className="lead ml-5">
            <p>Hello {user.name}</p>
            <p>Email: {user.email}</p>
            <p>About: {user.about}</p>
            <p>{`Joined ${new Date(user.created).toDateString()}`}</p>
          </div>
          {isAuthenticated().user && isAuthenticated().user._id === user._id ? (
            <div className="d-flex mt-4">
              <Link to={`/post/create`} className="btn btn-primary me-3">
                Create Post
              </Link>
              <Link to={`/user/edit/${user._id}`} className="btn btn-success me-3">
                Edit Profile
              </Link>
              <DeleteUser userId={user._id} />
            </div>
          ) : (
            <FollowProfileBtn
              following={following}
              onBtnClick={clickFollowButton}
            />
          )}

          <hr />
        </div>
      </div>

      <div className="row">
        <div className="col md-12 mt-5 mb-5">
          <hr />
          <p className="lead">{user.about}</p>
          <hr />
          <ProfilesTab following={user.following} followers={user.followers} posts={posts} />
        </div>
      </div>
    </div>
  );
};

export default Profile;
