import React, { useState } from "react";
import { Link } from "react-router-dom";
import { signUpUser } from "../auth";

const Signup = () => {
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    error: "",
  });

  const [isSubmit, setIsSubmit] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleInputChange = (event) => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    const { name, email, password } = values;

    const user = {
      name,
      email,
      password,
    };

    if (isValid()) {
      setLoading(true)
      signUpUser(user)
        .then((res) => {
          setValues((values) => ({
            ...values,
            name: "",
            email: "",
            password: "",
            error: "",
          }));
          setIsSubmit(true);
          setLoading(false)
        })
        .catch((err) => {
          setValues((values) => ({
            ...values,
            error: err?.response?.data.error,
          }));
          setLoading(false)
        });
    }
  };

  const isValid = () => {
    const { name, email, password } = values;

    if (name.length === 0) {
      setValues({ ...values, error: "Name is required" });
      return false;
    }
    // email@domain.com
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      setValues({ ...values, error: "A valid Email is required" });
      return false;
    }
    if (password.length >= 1 && password.length <= 5) {
      setValues({
        ...values,
        error: "Password must be at least 6 characters long",
      });
      return false;
    }
    return true;
  };

  const signUpForm = () => (
    <form>
      <div className="form-group p-2">
        <label className="text-muted">Name</label>
        <input
          type="text"
          className="form-control"
          name="name"
          value={values.name}
          placeholder="Enter name"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Email</label>
        <input
          type="email"
          className="form-control"
          name="email"
          value={values.email}
          placeholder="Enter email"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Password</label>
        <input
          type="password"
          className="form-control"
          name="password"
          value={values.password}
          placeholder="Enter password"
          onChange={handleInputChange}
        />
      </div>
      <button onClick={onSubmitHandler} className="btn btn-primary mt-2">
        Submit
      </button>
    </form>
  );

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">Signup</h2>

      {values.error ? (
        <div className="alert alert-danger" role="alert">
          {values.error}
        </div>
      ) : (
        ""
      )}
      {isSubmit ? (
        <div className="alert alert-success" role="alert">
          Account created successfully, Please{" "}
          <Link className="text-primary text-decoration-none" to="/signin">
            Signin
          </Link>
          .
        </div>
      ) : (
        ""
      )}

{loading ? (
        <div className="d-flex align-items-center">
          <strong>Loading...</strong>
          <div
            className="spinner-border ms-auto"
            role="status"
            aria-hidden="true"
          ></div>
        </div>
      ) : (
        signUpForm()
      )}

      {/* {signUpForm()} */}
    </div>
  );
};

export default Signup;
