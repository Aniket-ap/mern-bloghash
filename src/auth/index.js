import axios from "axios";

export const isAuthenticated = () => {
  if (typeof window == "undefined") {
    return false;
  }

  if (localStorage.getItem("jwt")) {
    return JSON.parse(localStorage.getItem("jwt"));
  } else {
    return false;
  }
};

export const signUpUser = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/signup`, user);
};

export const signInUser = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/signin`, user);
};

export const signOutUser = () => {
  return axios.get(`${process.env.REACT_APP_API_URL}/signout`);
};

export const getUser = (userId) => {
  return axios.get(`${process.env.REACT_APP_API_URL}/user/${userId}`, {
    headers: { Authorization: `Bearer ${isAuthenticated().token}` },
  });
};

export const getAllUsers = () => {
  return axios.get(`${process.env.REACT_APP_API_URL}/users`);
};

export const userDelete = (userId) => {
  return axios.delete(`${process.env.REACT_APP_API_URL}/user/${userId}`, {
    headers: { Authorization: `Bearer ${isAuthenticated().token}` },
  });
};

export const updateUser = (userId, user) => {
  return axios.put(`${process.env.REACT_APP_API_URL}/user/${userId}`,user,
   { headers: { Authorization: `Bearer ${isAuthenticated().token}` } });
}

export const updateUserNameOnMenu = (user, next) => {
  if (typeof window !== "undefined") {
      if (localStorage.getItem("jwt")) {
          let auth = JSON.parse(localStorage.getItem("jwt"));
          auth.user = user;
          localStorage.setItem("jwt", JSON.stringify(auth));
          next();
      }
  }
}

export const follow = (userId, token, followId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/user/follow`, {
      method: "PUT",
      headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ userId, followId })
  })
      .then(response => {
          return response.json();
      })
      .catch(err => console.log(err));
};

export const unfollow = (userId, token, unfollowId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/user/unfollow`, {
      method: "PUT",
      headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ userId, unfollowId })
  })
      .then(response => {
          return response.json();
      })
      .catch(err => console.log(err));
};

export const findPeople = (userId, token) => {
  return fetch(`${process.env.REACT_APP_API_URL}/user/findpeople/${userId}`, {
      method: "GET",
      headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
      }
  })
      .then(response => {
          return response.json();
      })
      .catch(err => console.log(err));
};

// POST API
