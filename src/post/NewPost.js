import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth";
import { create } from "./apiPost";

const NewPost = (props) => {
  const [userValues, setUserValues] = useState({
    title: "",
    body: "",
    photo: "",
    error: "",
    user: {},
  });

  const [redirectToProfile, setRedirectToProfile] = useState(false);
  let [postData] = useState(new FormData());
  const [fileSize, setFileSize] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setUserValues({ ...userValues, user: isAuthenticated().user });
  }, []);

  function handleInputChange(event) {
    setUserValues({ ...userValues, error: "" });
    const value =
      event.target.name === "photo"
        ? event.target.files[0]
        : event.target.value;

    postData.set(event.target.name, value);
    setFileSize(event.target.name === "photo" ? event.target.files[0].size : 0);
    setUserValues({ ...userValues, [event.target.name]: value });
  }

  const onSubmitHandler = (event) => {
    event.preventDefault();
    setLoading(true);

    if (isValid()) {
      const userId = isAuthenticated().user._id;
      const token = isAuthenticated().token;

      create(userId, token, postData).then((data) => {
        if (data.error) setUserValues({ ...userValues, error: data.error });
        else {
          setLoading(false);
          setUserValues({ ...userValues, title: "", body: "", photo: "" });
          setRedirectToProfile(true);
        }
      });
    }
  };

  const isValid = () => {
    const { title, body } = userValues;

    if (fileSize > 100000) {
      setUserValues({
        ...userValues,
        error: "File size should be less then 100kb",
      });
      return false;
    }

    if (title.length === 0 || body.length === 0) {
      setUserValues({ ...userValues, error: "All fields are required" });
      return false;
    }

    return true;
  };

  const newPostForm = () => (
    <form>
      <div className="form-group p-2">
        <label className="text-muted">Profile Photo</label>
        <input
          type="file"
          className="form-control"
          name="photo"
          accept="image/*"
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Title</label>
        <input
          type="text"
          className="form-control"
          name="title"
          value={userValues.title}
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group p-2">
        <label className="text-muted">Body</label>
        <textarea
          type="text"
          className="form-control"
          name="body"
          value={userValues.body}
          onChange={handleInputChange}
        />
      </div>
      <button onClick={onSubmitHandler} className="btn btn-primary mt-2">
        Create Post
      </button>
    </form>
  );

  if (redirectToProfile) {
    return <Redirect to={`/user/${userValues.user._id}`} />;
  }

  return (
    <div className="container">
      <h2 className="mt-5 mb-5">Create a new post</h2>

      {userValues.error ? (
        <div className="alert alert-danger" role="alert">
          {userValues.error}
        </div>
      ) : (
        ""
      )}

      {loading ? (
        <div className="d-flex align-items-center">
          <strong>Loading...</strong>
          <div
            className="spinner-border ms-auto"
            role="status"
            aria-hidden="true"
          ></div>
        </div>
      ) : (
        newPostForm()
      )}
    </div>
  );
};

export default NewPost;
