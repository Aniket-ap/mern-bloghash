import React, { useState, useEffect } from "react";
import { singlePost, remove, like, unlike } from "./apiPost";
import DefaultPost from "../assets/images/defaultBlog.jpg";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth/index";
import Comment from "./Comment";

import { AiOutlineLike, AiOutlineDislike } from "react-icons/ai";

const SinglePost = (props) => {
  const [post, setPost] = useState("");
  const [comments, setComments] = useState([]);
  const [redirectToHome, setRedirectToHome] = useState(false);
  const [redirectToSignin, setRedirectToSignin] = useState(false);
  const [postLikes, setPostLikes] = useState({
    like: false,
    likes: 0,
  });

  const checkLike = (likes) => {
    const userId = isAuthenticated() && isAuthenticated().user._id;
    let match = likes.indexOf(userId) !== -1;
    return match;
  };

  useEffect(() => {
    const postId = props.match.params.postId;
    singlePost(postId).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setPost(data);
        setPostLikes({
          ...postLikes,
          likes: data.likes.length,
          like: checkLike(data.likes),
        });
        setComments(data.comments);
      }
    });
  }, []);

  const updateComments = (comments) => {
    setComments({ comments });
  };

  const likeToggle = () => {
    if (!isAuthenticated()) {
      setRedirectToSignin(true);
      return false;
    }
    let callApi = postLikes.like ? unlike : like;
    const userId = isAuthenticated().user._id;
    const postId = post._id;
    const token = isAuthenticated().token;

    callApi(userId, token, postId).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setPostLikes({
          ...postLikes,
          like: !postLikes.like,
          likes: data.likes.length,
        });
      }
    });
  };

  const deletePost = () => {
    const postId = props.match.params.postId;
    const token = isAuthenticated().token;
    remove(postId, token).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setRedirectToHome(true);
      }
    });
  };

  const deleteConfirmed = () => {
    let answer = window.confirm("Are you sure you want to delete your post?");
    if (answer) {
      deletePost();
    }
  };

  const renderPost = (post) => {
    const posterId = post.postedBy ? `/user/${post.postedBy._id}` : "";
    const posterName = post.postedBy ? post.postedBy.name : " Unknown";

    return (
      <div className="card-body">
        <img
          src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
          alt={post.title}
          onError={(i) => (i.target.src = `${DefaultPost}`)}
          className="img-thunbnail mb-3"
          style={{
            height: "300px",
            width: "100%",
            objectFit: "cover",
          }}
        />

        {like ? (
          <h3 onClick={likeToggle}>
            <AiOutlineLike size={30} />
            {postLikes.likes} Like
          </h3>
        ) : (
          <h3 onClick={likeToggle}>
            <AiOutlineDislike size={30} />
            {postLikes.likes} Like
          </h3>
        )}

        <p className="card-text">{post.body}</p>
        <br />
        <p className="font-italic mark">
          Posted by <Link to={`${posterId}`}>{posterName} </Link>
          on {new Date(post.created).toDateString()}
        </p>
        <div className="d-inline-block">
          <Link to={`/`} className="btn btn-raised btn-primary btn-sm me-3">
            Back to posts
          </Link>
          {isAuthenticated().user &&
            isAuthenticated().user._id === post.postedBy._id && (
              <>
                <Link
                  to={`/post/edit/${post._id}`}
                  className="btn btn-raised btn-warning btn-sm me-3"
                >
                  Update Post
                </Link>
                <button
                  onClick={deleteConfirmed}
                  className="btn btn-raised btn-danger btn-sm"
                >
                  Delete Post
                </button>
              </>
            )}
        </div>
      </div>
    );
  };

  if (redirectToHome) {
    return <Redirect to={`/`} />;
  } else if (redirectToSignin) {
    return <Redirect to={`/signin`} />;
  }
  return (
    <div className="container">
      <h2 className="display-2 mt-5 mb-5">{post.title}</h2>

      {!post ? (
        <div className="d-flex align-items-center">
          <strong>Loading...</strong>
          <div
            className="spinner-border ms-auto"
            role="status"
            aria-hidden="true"
          ></div>
        </div>
      ) : (
        renderPost(post)
      )}

      {/* <Comment
        postId={post._id}
        comments={comments.reverse()}
        updateComments={updateComments}
      /> */}
    </div>
  );
};

export default SinglePost;
