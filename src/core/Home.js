import React from "react";
import { Link } from "react-router-dom";
import Posts from "../post/Posts";

const Home = () => {
  return (
    <div className="p-5 text-center bg-light">
      <h2 className="ml-50">Publish your passions, your way</h2>
      <p className="lead">
        Create a unique and beautiful blog, it's easy and free.
      </p>

      <Link to={`/post/create`} className="btn btn-primary">
        Write your first blog
      </Link>

      <div className="container">
        <Posts />
      </div>
    </div>
  );
};

export default Home;
