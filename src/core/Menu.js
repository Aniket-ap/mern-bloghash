import React, { useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { isAuthenticated, signOutUser } from "../auth";
import userlogo from "../assets/images/userProfileLogo.png";

import { FaBars, FaTimes } from "react-icons/fa";

import "./menu.css";

const isActive = (history, path) => {
  if (history.location.pathname === path)
    return { color: "#ff9900", borderBottom: "5px solid #ff9900" };
  else return { color: "#000" };
};

export const signout = (next) => {
  if (typeof window !== "undefined") localStorage.removeItem("jwt");
  next();

  signOutUser()
    .then((res) => {
      return res?.data.json();
    })
    .catch((err) => {
      console.log(err);
    });
};

const Menu = ({ history }) => {
  // const [click, setClick] = useState(false);
  // const handleClick = () => setClick(!click);

  return (
    <React.Fragment>
      <nav className="container main-nav">
        <div className="navbar-left">
          <Link className="navbar-brand" to="/" style={isActive(history, "/")}>
            BlogHash
          </Link>
        </div>

        <div className="navbar-right">
          <Link
            className="nav-link"
            to="/users"
            style={isActive(history, "/users")}
          >
            Users
          </Link>

          {!isAuthenticated() && (
            <React.Fragment>
              <Link
                className="nav-link"
                to="/signup"
                style={isActive(history, "/signup")}
              >
                Signup
              </Link>
              <Link
                className="nav-link"
                to="/signin"
                style={isActive(history, "/signin")}
              >
                Signin
              </Link>
            </React.Fragment>
          )}

          {isAuthenticated() && (
            <React.Fragment>
              <Link
                to={`/findpeople`}
                className="nav-link"
                style={isActive(history, `/findpeople`)}
              >
                Find people
              </Link>
              <Link
                to={`/post/create`}
                className="nav-link"
                style={isActive(history, `/post/create`)}
              >
                Create Post
              </Link>
              <Link
                to={`/user/${isAuthenticated().user._id}`}
                className="nav-link"
                style={isActive(history, `/user/${isAuthenticated().user._id}`)}
              >
                <img className="userProfileLogo" src={userlogo} alt="user" />
                {`${isAuthenticated().user.name}'s`}
              </Link>
              <span
                className="nav-link logout"
                style={isActive(history, "/signout")}
                onClick={() => signout(() => history.push("/"))}
              >
                Signout
              </span>
            </React.Fragment>
          )}

          {/* <div className="hamburger">
            <FaBars size={20}/>
          </div> */}
        </div>
      </nav>
    </React.Fragment>
  );
};

export default withRouter(Menu);
